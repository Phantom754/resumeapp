const btnToogler = document.getElementById("hamburger-btn");
const sideBar = document.querySelector(".tst-side");
btnToogler?.addEventListener("click", function () {
  sideBar.classList.toggle("active");
});
