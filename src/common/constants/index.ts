export class SvgConstant {
    static  readonly CAREER = "/assets/svg/career.svg";
    static  readonly RESUME = "/assets/svg/Resumizeme.svg";
    static  readonly STAR = "/assets/svg/star.svg";
}

export class ScriptConstant {
    static  readonly MOBILE_FIRST =  "/assets/script/tst.js";
}