import React from 'react'

const Footer = () => {
  return (
    <div className="tst-content__footer">
    <ul>
      <li>
        <a href="#" className="hover:text-blue">
          Terms & Conditions
        </a>
      </li>
      <li>
        <a href="#" className="hover:text-blue">
          Privacy Policy
        </a>
      </li>
      <li>
        <a href="#" className="hover:text-blue">
          FAQ
        </a>
      </li>
      <li>
        <a href="#" className="hover:text-blue">
          Contact Us
        </a>
      </li>
    </ul>
  </div>
  )
}

export default Footer