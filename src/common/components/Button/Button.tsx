import React, { FC } from "react";

type Props = {
    title: string;
}
const Button:FC<Props> = ({title}) => {
  return (
    <div className="col-12 col-lg-12">
      <button className="tst-btn tst-btn-blue text-white w-20">{title}</button>
    </div>
  );
};

export default Button;
