import { SvgConstant } from '@/common/constants'
import React from 'react'

const Card = () => {
  return (
    <div className="card-premium-account bg-primary br-sm mb-5">
    <div className="icon">
      <div className="tst-image">
        <img 
           alt=""
           src={SvgConstant.CAREER}
        />
      </div>
    </div>
    <div className="card-premium-account__content">
      <div className="title">Premium Account</div>
      <div className="desc font-normal">
        <p>
           You have a premium account, granting you access to all the remarkable features offered by ResumeDone. With this privilege, 
           you can indulge in the freedom of downloading an unlimited number of resumes and cover letters in both PDF and Word formats.
        </p>
      </div>
    </div>
  </div>
  )
}

export default Card