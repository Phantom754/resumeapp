//import Image from "next/image";
// import variables from '@/common/styles/variables.module.scss'

import SideBar from "@/common/layouts/Sidebar";
import Footer from "@/common/layouts/footer";
import HomeWrapper from "@/modules/Home/homeWrapper";


export default function Home() {
  return (
    <div className="tst-app">
      <SideBar />
      <div className="tst-content">
        <HomeWrapper />
        <Footer />
      </div>
    </div>
  );
}
